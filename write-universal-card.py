#!env/bin/python

import requests
import json
import RPi.GPIO as GPIO
import MFRC522
import signal

from settings import GESTOR_HOST
from settings import OFFLINE

__author__ = "José Luis Castro Sola @calambrenet www.codefriends.es"
__license__ = "GPL"
__email__ = "calambrenet@codefriends.es"
__status__ = "Production"

continue_reading = True

# Capture SIGINT for cleanup when the script is aborted
def end_read(signal,pframe):
    global continue_reading
    print("Ctrl+C captured, ending read.")
    continue_reading = False
    GPIO.cleanup()

def main():
    print("----- Grabador de tarjetas universales NFC -----")
    if OFFLINE:
        print("¡Atención! El sistema está en modo offline. No es posible usar tarjetas universales con el modo OFFLINE.")
        return

    while(1):
        company_id = input("Indicar el id de la empresa: ")
        if company_id == '':
            print("¡Error! Es necesario indicar un valor")
        else:
            break

        #while(1):
        #worker_id = input("Indicar el id del trabajador: ")
            #if worker_id == '':
        #print("¡Error! Es necesario indicar un valor")
            #else:
        #break

    print("Un momento, por favor...")
    r = requests.post(url = GESTOR_HOST + '/api/v1/terminal/generate_company_fcmcode/', verify=False, data = {'company_id': company_id})
    if r.status_code != 200:
        print("¡Error! Parece que el trabajador o la empresa no existe.")
        return

    try:
        response = json.loads(r.text)
    except Exception as e:
        print("¡Error! Ocurrió un error inesperado de tipo 1.")
        return

    if response['status'] != 'success':
        print("¡Error! Ocurrió un error inesperado de tipo 2.")
        return

    print("Generado código de tarjeta universal para empresa {}".format(response['company']))
    print("Por favor, acerque la tarjeta cerca del lector")

    code = response['code']
    code = code.split(",")
    print("Código: " + response['code'])

    # Create an object of the class MFRC522
    MIFAREReader = MFRC522.MFRC522()

    continue_reading = True
    while continue_reading:
        # print("buscando")

        # Scan for cards
        (status,TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)

        # If a card is found
        if status == MIFAREReader.MI_OK:
            print("Tarjeta detectada")

        # Get the UID of the card
        (status,uid) = MIFAREReader.MFRC522_Anticoll()

        # If we have the UID, continue
        if status == MIFAREReader.MI_OK:
            # Print UID
            print("Card read UID: %s,%s,%s,%s" % (uid[0], uid[1], uid[2], uid[3]))

            # This is the default key for authentication
            key = [0xFF,0xFF,0xFF,0xFF,0xFF,0xFF]

            # Select the scanned tag
            MIFAREReader.MFRC522_SelectTag(uid)

            # Authenticate
            status = MIFAREReader.MFRC522_Auth(MIFAREReader.PICC_AUTHENT1A, 8, key, uid)
            print("\n")

            # Check if authenticated
            if status == MIFAREReader.MI_OK:

                # print("Sector 8 looked like this:")
                # Read block 8
                # MIFAREReader.MFRC522_Read(8)
                # print("\n")

                data = []
                for c in code:
                    data.append(int(c)) # Pasarlo a integer

                # Write the data
                MIFAREReader.MFRC522_Write(8, data)
                print("\n")

                print("Nuevo código generado:")
                # Check to see if it was written
                try:
                    (savedBlockAddr, savedBackData) = MIFAREReader.MFRC522_Read(8)
                except:
                    savedBackData = None

                print("\n")
                if savedBackData is None:
                    print("ERROR! El Identificador guardado en la tarjeta no es válido.")
                    print("Es necesario repetir el proceso.")
                elif savedBackData != data:
                    print("ERROR! El Identificador guardado en la tarjeta no concuerda con el Identificador de la empresa.")
                    print("Es necesario repetir el proceso.")
                else:
                    print("La tarjeta fué codificada con éxito.")

                continue_reading = False
            else:
                print("Error: Authentication error. Intentar de nuevo...")


if __name__ == '__main__':
    signal.signal(signal.SIGINT, end_read)
    main()
