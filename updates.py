# -*- coding: utf8 -*-

import os
import git
import time
import pip
import subprocess
import sys

__author__ = "José Luis Castro Sola @calambrenet www.codefriends.es"
__license__ = "GPL"
__email__ = "calambrenet@codefriends.es"
__status__ = "Production"


def check_updates(restart_time = 5):
    print("Buscar nuevos cambios")
    repo = git.Repo('./')

    # Limpiar cambios que pudieran existir
    repo.git.reset('--hard')

    master = repo.head.reference
    current_id = master.commit.hexsha

    origin = repo.remotes.origin

    try:
        origin.pull()
    except Exception as e:
        print("Error al hacer pull: " + str(e))
        raise Exception("Error al hacer pull")


    master = repo.head.reference
    new_id = master.commit.hexsha

    if new_id != current_id:
        print("Existen nuevos cambios.")
        print("Instalar requests")
        current_path = os.getcwd()
        subprocess.check_call([sys.executable, "-m", "pip", "install", "-r", "{}/requirements.txt".format(current_path)])
        print("OK")

        if restart_time is not None:
            print("Reiniciar en {} segundos".format(restart_time))
            time.sleep(restart_time)
            os.system('sudo shutdown -r now')
    else:
        print("Sin cambios")
