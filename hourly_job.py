#!env/bin/python
# -*- coding: utf8 -*-
"""
Ejecutar tareas cada hora.
"""

from still_alive import still_alive

__author__ = "José Luis Castro Sola @calambrenet www.codefriends.es"
__license__ = "GPL"
__email__ = "calambrenet@codefriends.es"
__status__ = "Production"

def main():
    print("hourly job")

    still_alive()

if __name__ == '__main__':
    main()