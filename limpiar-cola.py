#!env/bin/python
# -*- coding: utf8 -*-

import sqlite3


def limpiar_cola():
    conn = sqlite3.connect('regislab.db')
    cursor = conn.cursor()
    cursor.execute('DELETE FROM registries_queue')
    conn.commit()
    conn.close()

if __name__ == '__main__':
    limpiar_cola()
    print('Cola de registros limpiada')

