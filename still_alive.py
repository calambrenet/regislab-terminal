#!env/bin/python
# -*- coding: utf8 -*-

import datetime
import requests
from settings import GESTOR_HOST, DEVICE_ID, VERSION


__author__ = "José Luis Castro Sola @calambrenet www.codefriends.es"
__license__ = "GPL"
__email__ = "calambrenet@codefriends.es"
__status__ = "Production"

def still_alive():
	print("Notificación I'm still alive")

	url = GESTOR_HOST + '/api/v1/terminal/still_alive/'
	data = {
            'device': DEVICE_ID,
            'version': VERSION,
            'timestamp': int(datetime.datetime.now().timestamp())
    }

	try:
		r = requests.post(url = url, verify=False, data = data)
		print("Status code: " + str(r.status_code))
	except Exception as e:
		print("Error al realizar petición a servidor: " + str(e))

	print("FIN")
