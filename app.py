#!env/bin/python
# -*- coding: utf8 -*-

import RPi.GPIO as GPIO
import MFRC522
from tkinter import *

import locale
from PIL import ImageTk, Image
import os
import signal
import time
import requests
import json
import datetime
import threading
import math
import sqlite3
import socket


from updates import check_updates

from settings import DEFAULT_COMPANIES, ENABLE_BARCODE_READER, ERROR_STOCK_SCREEN_WAIT_TO_STARTSCREEN, GESTOR_HOST, SLEEP_APP_WAIT_LOOP, DEVICE_ID, SUCCESS_STOCK_SCREEN_WAIT_TO_STARTSCREEN, VERSION, START_SCREEN_WAIT_TO_READCARD, \
    SUCCESS_SCREEN_WAIT_TO_STARTSCREEN, ERROR_SCREEN_WAIT_TO_STARTSCREEN, BEEPER, VERSION, \
    START_SCREEN_TIMER_UPDATE_TIME, TIKS_TO_WAIT_BARCODEREADER, DOORS_RELAY_ID, DOORS_RELAY, DOORS_RELAY_CLOSE_TIMEOUT, LOGO, \
    USE_ACTIONS, ACTIONS_SCREEN_WAIT_SECONDS, OFFLINE, CENTRO_TRABAJO_ID

__author__ = "José Luis Castro Sola @calambrenet www.codefriends.es"
__license__ = "GPL"
__email__ = "calambrenet@codefriends.es"
__status__ = "Production"

class Fullscreen_Window:
    title="Regislab"
    continue_reading = False
    MFRCReader = None
    time_label = None
    barcode_label = None
    BARCODE = None
    BARCODE_PROCESS = None
    CURRENT_PRODUCT = None
    PRODUCTS_LIST = []
    barcode_counter = 0
    manual_button = None
    manual_entry = None
    last_stock_event = 'EXI'    # además es el tipo por defecto

    barcode_label_success_text = 'El código de barras leído pertenece al  producto: \n{}. \nPor favor, indique el tipo de acción a registrar\n y pase su tarjeta de registros para procesar\n la petición'
    barcode_label_error_text = 'El código de barras leído no pertenece a \nningún producto.'
    actions_screen_counter = None
    back_to_start_screen_handle = None
    select_worker_screen = None

    internet_connection = False
    exist_queue_data = False
    db_conn = None


    def __init__(self):
        self.tk = Tk()
        self.tk.geometry("600x400")
        self.tk.title(self.title)
        self.tk.config(cursor="none")
        self.frame = Frame(self.tk)
        self.frame.pack()
        self.state = True
        self.tk.attributes("-fullscreen", True)
        self.tk.bind("<F12>", self.toggle_fullscreen)
        self.tk.bind("<Escape>", self.end_fullscreen)

        self.tk.bind("<F1>", self.render_start_screen)
        self.tk.bind("<F2>", self.render_error_screen)
        self.tk.bind("<F3>", self.render_success_screen)

        self.MFRCReader = MFRC522.MFRC522()

        self.tk.protocol("WM_DELETE_WINDOW", self.on_closing)

        if OFFLINE:
            self.init_database()

        self.render_start_screen()

    def on_closing(self):
        print("Cerrando aplicación")
        self.continue_reading = False
        GPIO.cleanup()
        self.tk.destroy()

    def init_database(self):
        print("Init local database")

        self.db_conn = sqlite3.connect('regislab.db')
        q = '''CREATE TABLE IF NOT EXISTS registries_queue (
            id INTEGER PRIMARY KEY,
            created timestamp not null,
            data text NOT NULL
            );'''

        cursor = self.db_conn.cursor()
        cursor.execute(q)
        self.db_conn.commit()
        cursor.close()

    def save_registry_queue(self, data):
        if self.db_conn is None:
            return

        cursor = self.db_conn.cursor()
        cursor.execute('INSERT INTO registries_queue (created, data) VALUES (?, ?)', (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), data))
        self.db_conn.commit()
        cursor.close()

    def clean_registry_queue(self):
        if self.db_conn is None:
            return

        cursor = self.db_conn.cursor()
        cursor.execute('DELETE FROM registries_queue')
        self.db_conn.commit()
        cursor.close()

    def get_registries_queue(self):
        if self.db_conn is None:
            return []

        cursor = self.db_conn.cursor()
        q = f'SELECT id, data, created from registries_queue'
        cursor.execute(q)
        rows = cursor.fetchall()
        cursor.close()

        registries = []
        for row in rows:
            registries.append({
                'registry': json.loads(row[1]),
                'created': row[2]
                })

        return registries

    def clean_registries_queue(self):
        if self.db_conn is None:
            return

        cursor = self.db_conn.cursor()
        q = f'DELETE from registries_queue'
        cursor.execute(q)
        self.db_conn.commit()
        cursor.close()

    def clear(self):
        for widget in self.tk.winfo_children():
            widget.destroy()

        self.time_label = None
        self.BARCODE = None

        self.tk.update()

    def postRegistry(self, code, extra_params = {}):
        self.actions_screen_counter = None

        loading_label = Label(self.tk, bg="white", text="Un momento...")
        loading_label.config(font=('Arial', 30), background="lightblue")
        loading_label.place(relx=0.5, rely=0.50, anchor='s')
        self.tk.update()

        url = GESTOR_HOST + '/api/v1/terminal/registry_worker_fcmcode/'
        extra_info = "terminal-{}-{}".format(DEVICE_ID, VERSION)

        print("POST url:" + url)

        data = {
            'code': ",".join(map(str,code)),
            'extra_info': extra_info,
            'actions': USE_ACTIONS,
            **extra_params
        }

        if CENTRO_TRABAJO_ID is not None:
            data['centro_trabajo_id'] = CENTRO_TRABAJO_ID

        json_data = {}

        if self.BARCODE:
            url = GESTOR_HOST + '/api/v1/terminal/registry_worker_barcodes/'
            json_data['bar_codes'] = self.PRODUCTS_LIST
            json_data['data'] = {
                'code': code,
                'extra_info': extra_info
            }
            data = None

        self.BARCODE = None
        self.BARCODE_PROCESS = False
        self.CURRENT_PRODUCT = None
        self.PRODUCTS_LIST = []

        if OFFLINE: 
            if not self.internet_connection or self.exist_queue_data:
                self.save_registry_queue(json.dumps(data))

                saved = self.get_registries_queue()
                print(saved)
                return {
                    'status': 'success',
                    'response': 'offline',
                    'worker': 'Trabajador',
                    'company': '',
                    'type': 'OFF',
                    'datetime': datetime.datetime.now().isoformat(),
                }


        try:
            r = requests.post(url = url, verify=False, data = data, json=json_data)
        except Exception as e:
            print("Error al realizar petición a servidor: " + str(e))
            return False

        print(r.text)

        if r.status_code != 200:
            print("¡Error! No se ha podido hacer el registro.")
            print("status_code: " + str(r.status_code))
            return False

        try:
            response = json.loads(r.text)
        except Exception as e:
            print("¡Error! No se ha podido cargar la respuesta en json: " + str(e))
            return False

        loading_label.destroy()
        self.tk.update()

        return response

    def checkInternetConnection(self):
        if not OFFLINE:
            self.internet_connection = True
            self.exist_queue_data = False
            return None

        has_changed = False

        #comprobar que no tenemos datos en la cola
        registries = self.get_registries_queue()
        if len(registries) > 0:
            self.exist_queue_data = True
        else:
            self.exist_queue_data = False

        try:
            # Intenta conectarse a un servidor de Google
            socket.create_connection(("www.google.com", 80))
            if self.internet_connection != True:
                has_changed = True

            self.internet_connection = True

        except OSError:
            if self.internet_connection != False:
                has_changed = True
            self.internet_connection = False

        return has_changed

    
    def postRegistriesQueue(self):
        self.clear()

        self.actions_screen_counter = None
        self.continue_reading = False

        loading_label = Label(self.tk, bg="white", text="Sincronizando...")
        loading_label.config(font=('Arial', 30), background="lightblue")
        loading_label.place(relx=0.5, rely=0.50, anchor='s')
        self.tk.update()

        url = GESTOR_HOST + '/api/v1/terminal/registries_queue/'
        print("POST url:" + url)

        data = {
            'data': self.get_registries_queue()
        }

        try:
            r = requests.post(url = url, verify=False, json=data)
        except Exception as e:
            print("Error al realizar petición a servidor: " + str(e))
            self.render_start_screen()
            return

        print(r.text)

        if r.status_code != 200:
            print("¡Error! No se ha podido hacer el registro.")
            print("status_code: " + str(r.status_code))

        try:
            response = json.loads(r.text)
        except Exception as e:
            print("¡Error! No se ha podido cargar la respuesta en json: " + str(e))
            self.render_start_screen()
            return

        self.clean_registry_queue()
        self.checkInternetConnection()
        self.render_start_screen()


    def wait_card(self):
        if not self.MFRCReader:
            print("No MFC Module")
            return

        print("wait_card")
        self.continue_reading = True
        tiks = 0
        while self.continue_reading:
            if self.BARCODE is not None:
                self.barcode_counter = self.barcode_counter + 1
                if self.barcode_counter > TIKS_TO_WAIT_BARCODEREADER:
                    self.barcode_counter = 0
                    self.disableBarCode()
                else:
                    self.countdownBarCode()

            # print("esperando")
            if OFFLINE:
                if tiks <= 0:
                    has_changed = self.checkInternetConnection()
                    if has_changed:
                        print("Redibujar pantalla")
                        self.render_start_screen()

                    if not self.internet_connection or self.exist_queue_data:
                        label = 'OFFLINE' 
                    else:
                        label = ' ONLINE '

                    conn_label = Label(self.tk, bg="white", text=label, font= ('Helvetica 15 underline'))
                    conn_label.place(relx=0.5, rely=0.1, anchor='s')

                    tiks = 4
                else:
                    tiks = tiks - 1

            # Scan for cards
            (status,TagType) = self.MFRCReader.MFRC522_Request(self.MFRCReader.PICC_REQIDL)

            # If a card is found
            if status == self.MFRCReader.MI_OK:
                print("Card detected")

            # Get the UID of the card
            (status,uid) = self.MFRCReader.MFRC522_Anticoll()

            # If we have the UID, continue
            if status == self.MFRCReader.MI_OK:

                # Print UID
                print("Card read UID: %s,%s,%s,%s" % (uid[0], uid[1], uid[2], uid[3]))

                # This is the default key for authentication
                key = [0xFF,0xFF,0xFF,0xFF,0xFF,0xFF]

                # Select the scanned tag
                self.MFRCReader.MFRC522_SelectTag(uid)

                # Authenticate
                status = self.MFRCReader.MFRC522_Auth(self.MFRCReader.PICC_AUTHENT1A, 8, key, uid)

                # Check if authenticated
                if status == self.MFRCReader.MI_OK:
                    data = self.MFRCReader.MFRC522_Read(8)
                    self.MFRCReader.MFRC522_StopCrypto1()

                    if data is not None:
                        print("Data: " + str(data[1]))
                        self.continue_reading = False
                        GPIO.cleanup()

                        response = self.postRegistry(data[1])
                        if response is False:
                            self.render_error_screen()
                        else:
                            if response['status'] != 'success':
                                if response['response'] == 'stock':
                                    self.render_error_stock_screen(data=response)
                                else:
                                    self.render_error_screen()
                            else:
                                if response['response'] == 'stock':
                                    self.render_success_stock_screen(data=response)
                                elif response['response'] == 'actions':
                                    self.actions_screen_counter = ACTIONS_SCREEN_WAIT_SECONDS
                                    self.render_actions(data=response, code=data[1])
                                elif response['response'] == 'universal_card':
                                    self.beepOK()
                                    self.render_select_worker_screen(code=data[1], data=response['data'])
                                else:
                                    self.render_success_screen(data=response)

                else:
                    print("Authentication error")

            try:
                self.tk.update_idletasks()
                self.tk.update()
            except:
                print("EXIT")
                self.continue_reading=False
                GPIO.cleanup()

            time.sleep(SLEEP_APP_WAIT_LOOP)


    def toggle_fullscreen(self, event=None):
        self.state = not self.state  # Just toggling the boolean
        self.tk.attributes("-fullscreen", self.state)
        return "break"

    def end_fullscreen(self, event=None):
        self.state = False
        self.tk.attributes("-fullscreen", False)
        return "break"

    def render_start_screen(self, event=None):
        self.clear()

        self.manual_entry = None
        self.select_worker_screen = None

        self.viewWindow = Canvas(self.tk, bg="white")
        self.viewWindow.pack(side=TOP, fill=BOTH, expand=True)

        self.tk.update()
        width = self.viewWindow.winfo_width()
        height = self.viewWindow.winfo_height()

        imageFile = Image.open(os.path.dirname(os.path.abspath(__file__)) + "/assets/" +  LOGO)
        imageFile = ImageTk.PhotoImage(imageFile)

        self.viewWindow.image = imageFile
        self.viewWindow.create_image(width/2, height/2, anchor=CENTER, image=imageFile, tags="bg_img")

        self.time_label = Label(self.tk, bg="white", text=time.strftime("%a, %d %b %Y %H:%M"))
        self.time_label.config(font=('Arial', 12))
        self.time_label.place(relx=0.8, rely=0.1, anchor='s')

        label = Label(self.tk, bg="white", text="V" + VERSION)
        label.config(font=('Arial', 8))
        label.place(relx=0.1, rely=0.1, anchor='s')

        label = Label(self.tk, bg="white", text='Registro de trabajadores/as')
        label.config(font=('Arial', 30))
        label.place(relx=0.5, rely=0.2, anchor='s')

        label = Label(self.tk, bg="white", text='Por favor, acerque su tarjeta personal\nal dispositivo para realizar el registro')
        label.config(font=('Arial', 20))
        label.place(relx=0.5, rely=0.85, anchor='s')

        if self.internet_connection and self.exist_queue_data:
            button = Button(self.tk, text="Sincronizar registros", command=self.postRegistriesQueue)
            button.place(relx=0.5, rely=0.97, anchor='s', width=200, height=60)
        elif ENABLE_BARCODE_READER and DEFAULT_COMPANIES is not None:
            self.entry = Entry(self.tk, width=2)
            self.entry.lower(self.viewWindow)

            self.entry.place(relx=.5, rely=.5, anchor="c")
            self.entry.focus()

            self.manual_button = Button(self.tk, text="Entrada código manual", command=self.set_stock_manual)
            self.manual_button.place(relx=0.5, rely=0.97, anchor='s', width=200, height=60)

            self.tk.bind('<Return>', self.enableBarCode)

        self.tk.after(START_SCREEN_WAIT_TO_READCARD, self.wait_card)
        self.tk.after(START_SCREEN_TIMER_UPDATE_TIME, self.updateTimeLabel)


    def set_stock_manual(self, event=None):
        self.manual_entry = Entry(self.tk, width=40)
        self.manual_entry.place(relx=.5, rely=.92, anchor="c")
        self.manual_entry.focus()

        if self.manual_button is not None:
            self.manual_button.destroy()

        self.tk.update()


    def enableBarCode(self, event=None):
        loading_label = Label(self.tk, bg="white", text="Un momento...")
        loading_label.config(font=('Arial', 30), background="lightblue")
        loading_label.place(relx=0.5, rely=0.50, anchor='s')
        self.tk.update()

        print("enableBarCode")

        if self.manual_entry is None:
            self.BARCODE = self.entry.get()
        else:
            self.BARCODE = self.manual_entry.get()

        print("->" + self.BARCODE)

        self.entry.delete(0, 'end')
        self.barcode_counter = 0

        url = GESTOR_HOST + '/api/v1/terminal/get_barcode_info/'
        print("POST url:" + url)

        data = {
            'companies': ",".join(map(str,DEFAULT_COMPANIES)),
            'code': self.BARCODE,
            'device_id': DEVICE_ID
        }

        try:
            r = requests.post(url = url, verify=False, data = data)
        except Exception as e:
            r = None
            print("Error al realizar petición a servidor: " + str(e))

        try:
            response = json.loads(r.text)
            status = response['status']
        except Exception as e:
            print("¡Error! No se ha podido cargar la respuesta en json: " + str(e))
            r = None

        if r is None or status != 'success':
            r = None

        loading_label.destroy()
        self.tk.update()

        frame = Frame(self.tk)
        frame.place(x=0, rely=0.2, width=self.viewWindow.winfo_width(), height=self.viewWindow.winfo_width())

        if r is None or r.status_code != 200:
            self.BARCODE_PROCESS = False
            print("Error al obtener la información del código de barras")

            self.barcode_label = Label(self.tk, bg="white", text=self.barcode_label_error_text)
            self.barcode_label.config(font=('Arial', 20), background="yellow")
            self.barcode_label.place(relx=0.5, rely=0.60, anchor='s')

            cancel_button = Button(self.tk, text="Cancelar", command=self.set_stock_product_cancel)
            cancel_button.place(relx=0.5, rely=0.97, anchor='s', width=200, height=70)
        else:
            self.BARCODE_PROCESS = True
            self.CURRENT_PRODUCT = response['data']
            self.PRODUCTS_LIST.append({
                'pk': response['data']['pk'],
                'type': self.last_stock_event #'EXI'
            })
            print("Código de barras correcto")

            print(self.PRODUCTS_LIST)

            self.barcode_label = Label(self.tk, bg="white", text=self.barcode_label_success_text.format(self.CURRENT_PRODUCT['name']))
            self.barcode_label.config(font=('Arial', 20), background="yellow")
            self.barcode_label.place(relx=0.5, rely=0.55, anchor='s')

            entrada_button = Button(self.tk, text="Entrada de producto", command=self.set_stock_product_entrada)
            entrada_button.place(relx=0.3, rely=0.80, anchor='s', width=200, height=70)

            salida_button = Button(self.tk, text="Retirada de producto", command=self.set_stock_product_salid)
            salida_button.place(relx=0.7, rely=0.80, anchor='s', width=200, height=70)

            cancel_button = Button(self.tk, text="Cancelar", command=self.set_stock_product_cancel)
            cancel_button.place(relx=0.5, rely=0.97, anchor='s', width=200, height=70)

            if self.last_stock_event == 'EXI':
                self.set_stock_product_salid()
            else:
                self.set_stock_product_entrada()


    def disableBarCode(self):
        print("disableBarCode")

        self.BARCODE = None
        self.BARCODE_PROCESS = False
        self.CURRENT_PRODUCT = None
        self.PRODUCTS_LIST = []

        self.render_start_screen()


    def countdownBarCode(self):
        remaining = TIKS_TO_WAIT_BARCODEREADER - self.barcode_counter + 1
        if self.BARCODE_PROCESS is True:
            self.barcode_label.configure(text=self.barcode_label_success_text.format(self.CURRENT_PRODUCT['name']) + " (%d)" % remaining)
        else:
            self.barcode_label.configure(text=self.barcode_label_error_text + " (%d)" % remaining)


    def set_stock_product_entrada(self, event=None):
        label = Label(self.tk, text='Entrada')
        label.config(font=('Arial', 20), width=100)
        label.place(relx=0.5, rely=0.64, anchor='s')

        self.PRODUCTS_LIST[len(self.PRODUCTS_LIST)-1]['type'] = 'ENT'
        self.last_stock_event = 'ENT'


    def set_stock_product_salid(self, event=None):
        label = Label(self.tk, text='Retirada')
        label.config(font=('Arial', 20), width=100)
        label.place(relx=0.5, rely=0.64, anchor='s')

        self.PRODUCTS_LIST[len(self.PRODUCTS_LIST)-1]['type'] = 'EXI'
        self.last_stock_event = 'EXI'


    def set_stock_product_cancel(self, event=None):
        self.disableBarCode()


    def updateTimeLabel(self):
        if self.time_label:
            self.time_label.configure(text=time.strftime("%a, %d %b %Y %H:%M"))
            self.time_label.update()
            self.tk.after(START_SCREEN_TIMER_UPDATE_TIME, self.updateTimeLabel)


    def render_error_screen(self, event=None, msg=None):
        self.beepERROR()
        self.clear()

        self.viewWindow = Canvas(self.tk, bg="white")
        self.viewWindow.pack(side=TOP, fill=BOTH, expand=True)

        label = Label(self.tk, bg="white", text='¡Error!')
        label.config(font=('Arial', 44))
        label.place(relx=0.5, rely=0.2, anchor='s')

        label = Label(self.tk, bg="white", text=msg or 'No se ha podido\nrealizar el registro\ncorrectamente.')
        label.config(font=('Arial', 30))
        label.place(relx=0.5, rely=0.70, anchor='s')

        self.continue_reading = False

        self.tk.after(ERROR_SCREEN_WAIT_TO_STARTSCREEN, self.render_start_screen)

    def render_error_stock_screen(self, event=None, data=None):
        self.beepERROR()
        self.clear()

        self.viewWindow = Canvas(self.tk, bg="white")
        self.viewWindow.pack(side=TOP, fill=BOTH, expand=True)

        label = Label(self.tk, bg="white", text='¡Error!')
        label.config(font=('Arial', 44))
        label.place(relx=0.5, rely=0.2, anchor='s')

        label = Label(self.tk, bg="white", text='No se ha podido\nrealizar el control de stock\ncorrectamente.')
        label.config(font=('Arial', 30))
        label.place(relx=0.5, rely=0.60, anchor='s')

        label = Label(self.tk, bg="white", text=data['cause'])
        label.config(font=('Arial', 30), background='yellow')
        label.place(relx=0.5, rely=0.85, anchor='s')

        self.continue_reading = False

        self.tk.after(ERROR_STOCK_SCREEN_WAIT_TO_STARTSCREEN, self.render_start_screen)

    def render_success_screen(self, event=None, data=None):
        if data is None:
            data = {
                'worker': 'José Luis Castro',
                'company': 'Empresa de los montes largos y buenos',
                'type': 'ENT',
                'datetime': '2019-12-04T17:12:57.198894'
            }

        print("render_success_screen")

        self.beepOK()
        self.relayOpen()
        self.clear()

        self.viewWindow = Canvas(self.tk, bg="white")
        self.viewWindow.pack(side=TOP, fill=BOTH, expand=True)

        worker_name = data['worker'] if data['worker'] else '--'
        truncate_length = 20
        label = Label(self.tk, bg="white", text=(worker_name[:truncate_length] + '...') if len(worker_name) > truncate_length else worker_name)
        label.config(font=('Arial', 44))
        label.place(relx=0.5, rely=0.2, anchor='s')

        label = Label(self.tk, bg="white", text=data['company'])
        label.config(font=('Arial', 30))
        label.place(relx=0.5, rely=0.35, anchor='s')

        reg_datetime = datetime.datetime.fromisoformat(data['datetime'])
        if data['type'] == 'ENT':
            type = 'Entrada'
        elif data['type'] == 'EXI':
            type = 'Salida'
        elif data['type'] == 'ACT':
            type = 'Acción'
        elif data['type'] == 'OFF':
            type = 'Off-line'
        elif data['type'] == 'ENT-EXI':
            type = 'Salida y entrada'
        else:
            type = 'Presencia'

        label = Label(self.tk, bg="white", text=type)
        label.config(font=('Arial', 80))
        label.place(relx=0.5, rely=0.65, anchor='s')

        label = Label(self.tk, bg="white", text="Registro fecha y hora: {}".format(reg_datetime.strftime('%d-%m-%Y %H:%M:%S')))
        label.config(font=('Arial', 20))
        label.place(relx=0.5, rely=0.76, anchor='s')

        if 'worker_balance' in data and data['worker_balance'] != '' and data['worker_balance'] != 0:
            balance = data['worker_balance']
            negativo = balance > 0;
            hours = math.floor(balance / 60)
            minutes = balance % 60;
            if hours > 0:
                balance = str(hours) + ' horas y ' + str(minutes) + ' minutos'
            else:
                balance = str(minutes) + ' minutos'

            if negativo:
                option = ' a favor de la empresa.';
            else:
                option = ' a favor del trabajador.';

            label = Label(self.tk, bg="white", text="El saldo actual con tu empresa es de")
            label.config(font=('Arial', 16))
            label.place(relx=0.5, rely=0.84, anchor='s')

            label = Label(self.tk, bg="white", text=balance + option)
            label.config(font=('Arial', 16))
            label.place(relx=0.5, rely=0.90, anchor='s')

        self.continue_reading = False

        self.tk.after(SUCCESS_SCREEN_WAIT_TO_STARTSCREEN, self.render_start_screen)

    def render_success_stock_screen(self, event=None, data=None):
        print("render_success_stock_screen")

        if data is None:
            data = {
                'worker': 'José Luis Castro',
                'company': 'Empresa de los montes largos y buenos',
                'type': 'ENT',
                'datetime': '2019-12-04T17:12:57.198894',
                'product': 'nombre del producto',
            }

        self.beepOK()
        self.clear()

        self.viewWindow = Canvas(self.tk, bg="white")
        self.viewWindow.pack(side=TOP, fill=BOTH, expand=True)

        worker_name = data['worker'] if data['worker'] else '--'
        truncate_length = 20
        label = Label(self.tk, bg="white", text=(worker_name[:truncate_length] + '...') if len(worker_name) > truncate_length else worker_name)
        label.config(font=('Arial', 44))
        label.place(relx=0.5, rely=0.2, anchor='s')

        label = Label(self.tk, bg="white", text=data['company'])
        label.config(font=('Arial', 30))
        label.place(relx=0.5, rely=0.35, anchor='s')

        reg_datetime = datetime.datetime.fromisoformat(data['datetime'])

        label = Label(self.tk, bg="white", text="Control de stock completado")
        label.config(font=('Arial', 35), background="yellow")
        label.place(relx=0.5, rely=0.52, anchor='s')

        if data['msg']:
            label = Label(self.tk, bg="white", text=data['msg'])
        else:
            label = Label(self.tk, bg="white", text="Procesados {} productos".format(data['n_products']))

        label.config(font=('Arial', 30))
        label.place(relx=0.5, rely=0.75, anchor='s')

        label = Label(self.tk, bg="white", text="Movimiento de stock fecha y hora: {}".format(reg_datetime.strftime('%d-%m-%Y %H:%M:%S')))
        label.config(font=('Arial', 20))
        label.place(relx=0.5, rely=0.85, anchor='s')

        self.continue_reading = False

        self.tk.after(SUCCESS_STOCK_SCREEN_WAIT_TO_STARTSCREEN, self.render_start_screen)

    def render_actions(self, data, code):
        if data is None:
            data = {
                'worker': 'José Luis Castro',
                'company': 'Empresa de los montes largos y buenos',
                'type': 'ENT',
                'datetime': '2019-12-04T17:12:57.198894'
            }

        print("render_success_screen")

        self.beepOK()
        self.relayOpen()
        self.clear()

        self.viewWindow = Canvas(self.tk, bg="white")
        self.viewWindow.pack(side=TOP, fill=BOTH, expand=True)

        worker_name = data['worker'] if data['worker'] else '--'
        truncate_length = 20
        label = Label(self.tk, bg="white", text=(worker_name[:truncate_length] + '...') if len(worker_name) > truncate_length else worker_name)
        label.config(font=('Arial', 44))
        label.place(relx=0.5, rely=0.2, anchor='s')

        label = Label(self.tk, bg="white", text=data['company'])
        label.config(font=('Arial', 30))
        label.place(relx=0.5, rely=0.35, anchor='s')

        start_rely = rely = 0.50
        relx = 0.5
        n_buttons = 0
        if len(data['available_actions']) > 2:
            start_relx = relx = 0.31
        if len(data['available_actions']) > 4:
            start_relx = relx = 0.15

        for action in data['available_actions']:
            print("render action", str(action))
            truncate_length = 25
            if 'special' in action and action['special']:
                callback = lambda action=action: self.render_action_special(data, code, action)
            else:
                callback = lambda pk=action['pk']: self.postExitRegistry(code, {'force_action': pk})

            button = Button(self.tk, text=('*' if ('special' in action and action['special']) else '') 
                    + ((action['action'][:truncate_length] + '...') if len(action['action']) > truncate_length else action['action']), 
                    command=callback)
            button.place(relx=relx, rely=rely, anchor='s', width=200, height=50)

            n_buttons += 1
            rely += 0.17
            if n_buttons == 2:
                rely = start_rely
                relx += 0.35
            elif n_buttons == 4:
                rely = start_rely
                relx += 0.35
            elif n_buttons == 6:
                break

        self.render_actions_exit_button(data, code=code)

        self.tk.after(1200, self.render_actions_exit_button(data=data, code=code))

    def render_select_worker_screen(self, data, code, worker_id=None):
        #Mostra en pantalla usando tkinter un input de tipo text para que el usuario introduca el id de trabajador
        #para eso mostraremos un teclado numérico para que el usuario pueda introducir el id 
        #y un botón de aceptar para que el usuario pueda enviar el id introducido
        #también un botón para cancelar y volver a render_start_screen
        print("render_select_worker_screen")
        self.select_worker_screen = True
        self.current_company_data = data
        self.current_code = code

        self.clear()

        self.viewWindow = Canvas(self.tk, bg="white")
        self.viewWindow.pack(side=TOP, fill=BOTH, expand=True)

        label = Label(self.tk, bg="white", text='Introduce el id del trabajador')
        label.config(font=('Arial', 30))
        label.place(relx=0.5, rely=0.15, anchor='s')

        self.entry = Entry(self.tk, width=20, font=('Arial', 30), justify='center')

        self.entry.place(relx=.5, rely=.21, anchor="c")
        self.entry.focus()

        if worker_id is not None:
            self.entry.insert(END, worker_id)

        #teclado numérico
        self.render_numeric_keyboard()

        #botón de aceptar
        if worker_id is None or worker_id == '':
            self.enabled_kepad_buttons = True
            button = Button(self.tk, text="Aceptar", command=self.accept_worker)
            button.place(relx=0.7, rely=0.65, anchor='s', width=200, height=70)
        else:
            self.enabled_kepad_buttons = False
            button = Button(self.tk, text="Realizar registro", 
                            command=lambda: self.create_registry_from_worker_pk(code=code, company_pk=data['company_pk'], worker_pk=worker_id))
            button.place(relx=0.7, rely=0.65, anchor='s', width=200, height=70)

        #botón de Cancelar
        cancel_button = Button(self.tk, text="Cancelar", command=lambda: self.render_start_screen())
        cancel_button.place(relx=0.7, rely=0.90, anchor='s', width=200, height=70)

        #Si pasado 10 segundos no se ha introducido ningún id 
        #se vuelve a render_start_screen
        if self.back_to_start_screen_handle is not None:
            self.tk.after_cancel(self.back_to_start_screen_handle)
        self.back_to_start_screen_handle = self.tk.after(10000, self.back_to_start_screen)

    def back_to_start_screen(self):
        if self.select_worker_screen is True:
            self.render_start_screen()

    def create_registry_from_worker_pk(self, code, company_pk, worker_pk):
        response = self.postRegistryById(code, company_pk, worker_pk)

        if response is False:
            self.render_error_screen()
        else:
            if response['status'] != 'success':
                self.render_error_screen()
            else:
                self.render_success_screen(data=response)


    def postRegistryById(self, code, company_pk, worker_pk):
        loading_label = Label(self.tk, bg="white", text="Un momento...")
        loading_label.config(font=('Arial', 30), background="lightblue")
        loading_label.place(relx=0.5, rely=0.50, anchor='s')
        self.tk.update()

        url = GESTOR_HOST + '/api/v1/terminal/registry_worker_fcmcode/'
        extra_info = "terminal-{}-{}".format(DEVICE_ID, VERSION)

        print("POST url:" + url)

        data = {
            'code': ",".join(map(str, code)),
            'extra_info': extra_info,
            'company_pk': company_pk,
            'worker_pk': worker_pk,
        }


        json_data = {}

        try:
            r = requests.post(url = url, verify=False, data = data, json=json_data)
        except Exception as e:
            print("Error al realizar petición a servidor: " + str(e))
            return False

        print(r.text)

        if r.status_code != 200:
            print("¡Error! No se ha podido hacer el registro.")
            print("status_code: " + str(r.status_code))
            return False

        try:
            response = json.loads(r.text)
        except Exception as e:
            print("¡Error! No se ha podido cargar la respuesta en json: " + str(e))
            return False

        loading_label.destroy()
        self.tk.update()

        return response

    def render_numeric_keyboard(self):
        #renderiza el teclado numérico
        #cada botón tiene un command que llama a la función add_number
        #el tamaño de cada botón es de 60x60
        #y le pasa el número que le corresponde
        #el botón de borrar llama a la función delete_number
        #y el botón de borrar todo llama a la función delete_all
        #el botón de aceptar llama a la función accept_worker
        #y el botón de cancelar llama a la función render_start_screen
        #para que el usuario pueda introducir el id del trabajador
        print("render_numeric_keyboard")

        #fila 1
        button = Button(self.tk, text="1", command=lambda: self.add_number(1))
        button.place(relx=0.2, rely=0.56, anchor='s', width=60, height=60)

        button = Button(self.tk, text="2", command=lambda: self.add_number(2))
        button.place(relx=0.3, rely=0.56, anchor='s', width=60, height=60)

        button = Button(self.tk, text="3", command=lambda: self.add_number(3))
        button.place(relx=0.4, rely=0.56, anchor='s', width=60, height=60)

        #fila 2
        button = Button(self.tk, text="4", command=lambda: self.add_number(4))
        button.place(relx=0.2, rely=0.70, anchor='s', width=60, height=60)

        button = Button(self.tk, text="5", command=lambda: self.add_number(5))
        button.place(relx=0.3, rely=0.70, anchor='s', width=60, height=60)

        button = Button(self.tk, text="6", command=lambda: self.add_number(6))
        button.place(relx=0.4, rely=0.70, anchor='s', width=60, height=60)

        #fila 3
        button = Button(self.tk, text="7", command=lambda: self.add_number(7))
        button.place(relx=0.2, rely=0.84, anchor='s', width=60, height=60)

        button = Button(self.tk, text="8", command=lambda: self.add_number(8))
        button.place(relx=0.3, rely=0.84, anchor='s', width=60, height=60)

        button = Button(self.tk, text="9", command=lambda: self.add_number(9))
        button.place(relx=0.4, rely=0.84, anchor='s', width=60, height=60)

        #fila 4 
        button = Button(self.tk, text="Borrar", command=lambda: self.delete_number())
        button.place(relx=0.2, rely=0.99, anchor='s', width=60, height=60)

        button = Button(self.tk, text="0", command=lambda: self.add_number(0))
        button.place(relx=0.3, rely=0.99, anchor='s', width=60, height=60)

        button = Button(self.tk, text="B. todo", command=lambda: self.delete_all())
        button.place(relx=0.4, rely=0.99, anchor='s', width=60, height=60)

    def add_number(self, number):
        if self.enabled_kepad_buttons is False:
            return

        self.entry.insert(END, number)

    def delete_number(self):
        if self.enabled_kepad_buttons is False:
            return

        self.entry.delete(len(self.entry.get())-1)

    def delete_all(self):
        if self.enabled_kepad_buttons is False:
            return

        self.entry.delete(0, 'end')


    def accept_worker(self):
        id = self.entry.get().strip()

        worker_id = None
        for worker in self.current_company_data['workers']:
            if id != "" and worker['pk'] == int(id):
                worker_id = int(id)
                break

        self.clear()
        self.render_select_worker_screen(self.current_company_data, self.current_code, worker_id)

        if id == '':
            self.beepERROR()

            #si el entry está vacío
            #muestra un mensaje de Error
            label = Label(self.tk, bg="white", text='¡Error! El campo está vacío')
            label.config(font=('Arial', 25))
            label.place(relx=0.5, rely=0.40, anchor='s')
        else:
            id = int(id)
            for worker in self.current_company_data['workers']:
                if worker['pk'] == id:
                    self.beepOK()

                    label = Label(self.tk, bg="white", text=worker['name'])
                    label.config(font=('Arial', 30))
                    label.place(relx=0.5, rely=0.40, anchor='s')

                    return
            
            self.beepERROR()
            label = Label(self.tk, bg="white", text='¡Error! El id introducido no es correcto')
            label.config(font=('Arial', 25))
            label.place(relx=0.5, rely=0.40, anchor='s')
            self.entry.delete(0, 'end')


    def render_action_special(self, data, code, action):
        if data is None:
            data = {
                'worker': 'José Luis Castro',
                'company': 'Empresa de los montes largos y buenos',
                'type': 'ENT',
                'datetime': '2019-12-04T17:12:57.198894'
            }

        self.actions_screen_counter = None
        print("render_action_special")

        self.clear()

        self.viewWindow = Canvas(self.tk, bg="white")
        self.viewWindow.pack(side=TOP, fill=BOTH, expand=True)

        worker_name = data['worker'] if data['worker'] else '--'
        truncate_length = 20
        label = Label(self.tk, bg="white", text=(worker_name[:truncate_length] + '...') if len(worker_name) > truncate_length else worker_name)
        label.config(font=('Arial', 44))
        label.place(relx=0.5, rely=0.2, anchor='s')

        label = Label(self.tk, bg="white", text=data['company'])
        label.config(font=('Arial', 30))
        label.place(relx=0.5, rely=0.35, anchor='s')

        label = Label(self.tk, bg="white", text=action['action'])
        label.config(font=('Arial', 30))
        label.place(relx=0.5, rely=0.50, anchor='s')

        
        button = Button(self.tk, text='Abrir \n(El trabajador sale a realizar la acción)', 
                command=lambda pk=action['pk']: self.postExitRegistry(code, {'force_action': pk, 'force_type': 'ENT'}))
        button.place(relx=0.24, rely=0.66, anchor='s', width=260, height=55)

        button = Button(self.tk, text='Cerrar \n(El trabajador se incorpora al trabajo)', 
                command=lambda pk=action['pk']: self.postExitRegistry(code, {'force_action': pk, 'force_type': 'EXI'}))
        button.place(relx=0.77, rely=0.66, anchor='s', width=260, height=55)

        cancel_button = Button(self.tk, text="Cancelar", command=lambda: self.render_start_screen())
        cancel_button.place(relx=0.5, rely=0.97, anchor='s', width=200, height=70)


    def render_actions_exit_button(self, data, code):
        if self.actions_screen_counter is None:
            return

        if self.actions_screen_counter > 0:
            self.tk.update()

            if self.actions_screen_counter > 3:
                callback = lambda: self.postExitRegistry(code, {'force_action': 'exit'})
            else:
                callback = lambda: print("No llamar callback")

            button = Button(self.tk, text='Salida ' + '(' + str(int(self.actions_screen_counter)) + ')', 
                    command=callback, bg='#0066b2', fg='#ffffff')
            button.place(relx=0.5, rely=0.90, anchor='s', width=200, height=50)

            self.actions_screen_counter = self.actions_screen_counter - 0.5

        if self.actions_screen_counter <= 0:
            self.actions_screen_counter = None
            #print("Hacer salida")
            #self.postExitRegistry(code, {'force_action': 'exit'})
            self.tk.after(1000, lambda: self.render_start_screen())
        else:
            self.tk.after(1000, lambda: self.render_actions_exit_button(data=data, code=code))

    def postExitRegistry(self, code, extra_params):
        self.actions_screen_counter = None

        response = self.postRegistry(code, extra_params)
        if response is False:
            self.render_error_screen()
        else:
            if response['status'] != 'success':
                if response['code'] == 'must_close_action':
                    msg = self.add_newline(response['msg'], 30)
                    self.render_error_screen(msg=msg)
                else:
                    self.render_error_screen()
            else:
                self.render_success_screen(data=response)

    def add_newline(self, string, n):
        words = string.split()
        lines = []
        current_line = ""
        
        for word in words:
            if len(current_line) + len(word) <= n:
                current_line += word + " "
            else:
                lines.append(current_line.strip())
                current_line = word + " "
        
        lines.append(current_line.strip())
        return "\n".join(lines)

    def beepOK(self):
        if not BEEPER:
            return

        print("Beep OK")

        GPIO.cleanup()
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        buzzer=23

        GPIO.setup(buzzer,GPIO.OUT)
        GPIO.output(buzzer,GPIO.HIGH)
        time.sleep(0.2)
        GPIO.output(buzzer,GPIO.LOW)
        time.sleep(0.2)
        GPIO.output(buzzer,GPIO.HIGH)
        time.sleep(0.2)
        GPIO.output(buzzer,GPIO.LOW)
        GPIO.cleanup()

    def beepERROR(self):
        if not BEEPER:
            return

        print("Beep ERROR")

        GPIO.cleanup()
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        buzzer=23
        GPIO.setup(buzzer,GPIO.OUT)

        GPIO.output(buzzer,GPIO.HIGH)
        time.sleep(0.6)
        GPIO.output(buzzer,GPIO.LOW)
        GPIO.cleanup()


    def relayOpen(self):
        if not DOORS_RELAY:
            return

        print('Open relay id {}'.format(DOORS_RELAY_ID))

        GPIO.cleanup()
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)

        GPIO.setup(DOORS_RELAY_ID, GPIO.OUT)
        GPIO.output(DOORS_RELAY_ID, GPIO.HIGH)
        time.sleep(0.2)
        GPIO.output(DOORS_RELAY_ID, GPIO.LOW)

        thread_close = threading.Thread(target = app_close_relay)
        thread_close.start()


def app_close_relay():
    if not DOORS_RELAY:
        return

    time.sleep(DOORS_RELAY_CLOSE_TIMEOUT)
    print('Close relay id {}'.format(DOORS_RELAY_ID))

    GPIO.output(DOORS_RELAY_ID, GPIO.LOW)
    GPIO.cleanup()


def app_check_updates():
    print("Comprobar actualizaciones")
    for c in range(0,10):
        print("Intento {} de 10".format(c+1))
        try:
            check_updates(0)
        except:
            time.sleep(50)
            c = c+1
        else:
            break;

    return True

# Capture SIGINT for cleanup when the script is aborted
def end_read(signal,frame):
    print("Ctrl+C captured, ending read.")
    GPIO.cleanup()

signal.signal(signal.SIGINT, end_read)

if __name__ == '__main__':
    # check updated
    thread_updates = threading.Thread(target = app_check_updates)
    thread_updates.start()

    try:
        locale.setlocale(locale.LC_TIME, "es_ES.utf8")
    except:
        pass

    w = Fullscreen_Window()
    w.tk.mainloop()
    GPIO.cleanup()
