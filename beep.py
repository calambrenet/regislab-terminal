import RPi.GPIO as GPIO
from time import sleep

#Disable warnings (optional)
GPIO.setwarnings(False)
#Select GPIO mode
GPIO.setmode(GPIO.BCM)
#Set buzzer - pin 23 as output
buzzer=23
GPIO.setup(buzzer,GPIO.OUT)


print("OK")
GPIO.output(buzzer,GPIO.HIGH)
sleep(0.2)
GPIO.output(buzzer,GPIO.LOW)
sleep(0.2)
GPIO.output(buzzer,GPIO.HIGH)
sleep(0.2)
GPIO.output(buzzer,GPIO.LOW)

sleep(2)
print("Error")
GPIO.output(buzzer,GPIO.HIGH)
sleep(0.6)
GPIO.output(buzzer,GPIO.LOW)
print("End")
