#!env/bin/python
# -*- coding: utf8 -*-
"""
Ejecutar tareas diariamente. En principio se usará para sincronizar el código con el repositorio.
"""
from updates import check_updates

__author__ = "José Luis Castro Sola @calambrenet www.codefriends.es"
__license__ = "GPL"
__email__ = "calambrenet@codefriends.es"
__status__ = "Production"

def main():
    print("daily job")

    check_updates()

if __name__ == '__main__':
    main()
