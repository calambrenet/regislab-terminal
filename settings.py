# -*- coding: utf8 -*-

import local_settings

__author__ = "Jose Luis Castro Sola @calambrenet www.codefriends.es"
__license__ = "GPL"
__email__ = "calambrenet@codefriends.es"
__status__ = "Production"

GESTOR_HOST = 'https://gestor.gestiayuda.com'
#GESTOR_HOST = 'http://192.168.1.233:8000'
VERSION = '0.108'
SLEEP_APP_WAIT_LOOP = 0.8
TIKS_TO_WAIT_BARCODEREADER = 15
START_SCREEN_WAIT_TO_READCARD = 500
START_SCREEN_TIMER_UPDATE_TIME = 60000
ERROR_SCREEN_WAIT_TO_STARTSCREEN = 2000
ERROR_STOCK_SCREEN_WAIT_TO_STARTSCREEN = 6000
SUCCESS_SCREEN_WAIT_TO_STARTSCREEN = 1000
SUCCESS_STOCK_SCREEN_WAIT_TO_STARTSCREEN = 4000
DEVICE_ID = local_settings.DEVICE_ID

try:
    LOGO = local_settings.LOGO
except:
    LOGO = 'logo.png'

try:
    BEEPER = local_settings.BEEPER
except:
    BEEPER = True

try:
    DOORS_RELAY = local_settings.DOORS_RELAY
except:
    DOORS_RELAY = False

try:
    DOORS_RELAY_ID = local_settings.DOORS_RELAY_ID
except:
    DOORS_RELAY_ID = 18

try:
    DOORS_RELAY_CLOSE_TIMEOUT = local_settings.DOORS_RELAY_CLOSE_TIMEOUT
except:
    DOORS_RELAY_CLOSE_TIMEOUT = 4

try:
    ENABLE_BARCODE_READER = local_settings.ENABLE_BARCODE_READER
except:
    ENABLE_BARCODE_READER = False

try:
    DEFAULT_COMPANIES = local_settings.DEFAULT_COMPANIES
except:
    DEFAULT_COMPANIES = None


try:
    USE_ACTIONS = local_settings.USE_ACTIONS
except:
    USE_ACTIONS = True

try:
    ACTIONS_SCREEN_WAIT_SECONDS = local_settings.ACTIONS_SCREEN_WAIT_SECONDS
except:
    ACTIONS_SCREEN_WAIT_SECONDS = 11


try:
    OFFLINE = local_settings.OFFLINE
except:
    OFFLINE = False

try: 
    CENTRO_TRABAJO_ID = local_settings.CENTRO_TRABAJO_ID
except:
    CENTRO_TRABAJO_ID = None


print('CENTRO_TRABAJO_ID: ', CENTRO_TRABAJO_ID)
print ('USE_ACTIONS: ', USE_ACTIONS)

if USE_ACTIONS and CENTRO_TRABAJO_ID is not None:
    raise Exception('No se puede activar CENTROS DE TRABAJO y ACCIONES a la vez')
if USE_ACTIONS and OFFLINE:
    raise Exception('No se puede activar CENTROS DE TRABAJO y modo OFFLINE a la vez')

